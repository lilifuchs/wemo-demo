# wemo-demo

A smalll demo project using Angular to retreive sensor data using a HTTP GET request.

### Prerequisites

To run this project locally, you need npm and Node.js installed. Additionally, install
the Angular CLI with `npm install -g @angular/cli`.


### Running the project


Change to the rood directory of the project and run `ng serve --open`. This will open 
the web interface in your browser at [http://localhost:4200/](http://localhost:4200/)



## Built With

* [Angular](https://angular.io/) - The web framework used
* [NPM](https://www.npmjs.com/) - Package manager
* [Node.js](https://nodejs.org/en/) - Server environment


## Authors

* **Linda Kolb** - *Initial work* 


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

