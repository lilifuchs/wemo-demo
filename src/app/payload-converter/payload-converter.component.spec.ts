import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayloadConverterComponent } from './payload-converter.component';

describe('PayloadConverterComponent', () => {
  let component: PayloadConverterComponent;
  let fixture: ComponentFixture<PayloadConverterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayloadConverterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayloadConverterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
