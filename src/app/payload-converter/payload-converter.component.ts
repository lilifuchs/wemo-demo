import { Component, OnInit } from '@angular/core';
import { SensorinputService } from '../sensorinput.service';
import '../../assets/js/elsys';
import { SensorData } from '../SensorData';
declare var DecodeElsysPayload: any;
declare var hexToBytes: any;


@Component({
  selector: 'app-payload-converter',
  templateUrl: './payload-converter.component.html',
  styleUrls: ['./payload-converter.component.css']
})
export class PayloadConverterComponent implements OnInit {

  receivedData: SensorData = {
    temperature: 0,
    humidity: 0,
    light: 0,
    motion: 0,
    vdd: 0
  };

  lightThreshold = 10;
  motionThreshold = 10;
  error = '';
  plugIsOff = true;

  constructor(private sensorInputService: SensorinputService) { }


  static base64toHEX(payloadRaw: string): string {
    const decoded = atob(payloadRaw);
    let result = '';
    for (let i = 0; i < decoded.length; i++) {
      const hex = decoded.charCodeAt(i).toString(16);
      result = result  + (hex.length === 2 ? hex : '0' + hex);

    }
    return result.toUpperCase();

  }

  ngOnInit() {
    // TODO: implement loop to get data every x sec?
    this.getPayload();
    if (this.receivedData.light < this.lightThreshold) {
      this.plugIsOff = false;
    } else {
      this.plugIsOff = true;
    }



  }

  getPayload(): void {
    this.sensorInputService.getSensorInput()
      .subscribe(
       (data: Input) => {
         const payloadRaw = JSON.parse(data.blobJson)['payload_raw'];
         const decodedPayload = PayloadConverterComponent.base64toHEX(payloadRaw);
         return this.receivedData = DecodeElsysPayload(hexToBytes(decodedPayload));
       },
        error => this.error = error
      );
  }


}

export interface Input {
  blobJson: string;
  deviceMfg: string;
  eventCode: string;
  sensorClass: string;
  sensorUUID: string;
  timeStamp: string;
  uID: string;
}
