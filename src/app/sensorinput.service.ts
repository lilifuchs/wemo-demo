import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Input} from './payload-converter/payload-converter.component';

@Injectable({
  providedIn: 'root'
})
export class SensorinputService {

  constructor(private http: HttpClient) { }

  sensorInputUrl = 'https://ws1.chic.ulster.ac.uk/SensorCentral/REST/SensorData/A81758FFFE030813_100_100';


  getSensorInput() {

    return this.http.get<Input>(this.sensorInputUrl);
  }



}
