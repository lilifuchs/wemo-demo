import { TestBed } from '@angular/core/testing';

import { SensorinputService } from './sensorinput.service';

describe('SensorinputService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SensorinputService = TestBed.get(SensorinputService);
    expect(service).toBeTruthy();
  });
});
