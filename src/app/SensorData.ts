export class SensorData {
    temperature: number;
    humidity: number;
    light: number;
    motion: number;
    vdd: number;

}

